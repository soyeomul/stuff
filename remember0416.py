#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# 세월호 리본 -- 2014년 4월 16일
# 유니코드 코드 포인트: "U+1F397"
# 참조문헌: [1-2]
# [1] https://www.fileformat.info/info/unicode/char/1f397/index.htm
# [2] https://unicode.org/consortium/adopted-characters.html#b1F397

remember0416 = "🎗"
v = remember0416.encode("unicode_escape")

print(v)

# 편집: Emacs 26.1 (Ubuntu 18.04)
# 마지막 갱신: 2019년 4월 16일

"""
(bionic)soyeomul@localhost:~/test$ ./remember0416.py
b'\\U0001f397'
(bionic)soyeomul@localhost:~/test$ 
"""
