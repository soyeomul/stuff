#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# 그림 파일 출처: http://todayhumor.com/?sisa_1130081

import binascii
import subprocess

FURL = "https://gitlab.com/soyeomul/stuff/raw/master/%ED%95%9C%EC%9D%BC%EC%A0%84/%ED%95%9C%EC%9D%BC%EC%A0%84.jpg"
PRE_CMD = "rm -f /tmp/한일전.jpg"

subprocess.call(PRE_CMD + "; " + "wget -q -P /tmp " + FURL, shell=True)

with open("/tmp/한일전.jpg", "rb") as f:
    hexdata = binascii.hexlify(f.read())

print(hexdata)

# 편집: Emacs 26.1 (Ubuntu 18.04)
# 마지막 갱신: 2019년 4월 27일
