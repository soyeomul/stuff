# -*- coding: utf-8 -*-

import re
import subprocess

# 한자(漢字)를 한글로 변경하는 콤푸타 코드.
# 파이썬 3.7 필요, POSIX 환경에서만 실행할것.

# 인용문 출처: <모두의공원: 조국 장관의 출사표인 '서해맹산'이 이순신 장군님의 시이군요?!!>
# 인터넷주소: <URL:https://www.clien.net/service/board/park/13805466>
data = """\
天步西門遠
東宮北地危
孤臣憂國日
壯士樹勳時
誓海魚龍動
盟山草木知
讐夷如盡滅
雖死不爲辭
"""

print(data, end="") # 디버깅 위하야,,,

# 유니코드 한자 범위: (한중일 통합 한자)
r = "\U00004e00-\U00009fff"

def get_hanja(xyz): # 한자를 뽑아내는 함수
    chop = re.match("[{}]".format(r), xyz)
    if chop:
        return chop.group(0)

list_hanja = []; list_hanja_idx = []
for index in range(0, len(data)):
    hanja = get_hanja(data[index])
    if hanja:
        list_hanja.append(hanja)
        list_hanja_idx.append(index)

list_hanja = sorted(list(set(list_hanja))) # 결과 출력 시간 단축 ===> `set'

def get_ipa(xyz): # 한자에 해당하는 한글 찾는 함수 (시간 걸림)
    cmd = "curl -s https://ko.wiktionary.org/wiki/{}".format(xyz)
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE, text=True, shell=True)
    lines = p.stdout.readlines()

    exp = re.compile("한자 독음 ([가-힣])")
    for pdx in range(0, len(lines)):
        if "한자 독음" in lines[pdx]:
            ipa = exp.search(lines[pdx]).group(1)
            return ipa

"""
### 배경: https://ko.wiktionary.org/wiki/%E4%B8%8D 
참조: 不 뒤에 오는 낱말이 한국어로 'ㄷ', 'ㅈ'으로 발음되는 한자어일 때에는 «부»로 읽으며 그 이외에는 «불»로 읽는다.
===> 독불장군은 예외

### 산법출처: 
http://dream.ahboom.net/entry/%ED%95%9C%EA%B8%80-%EC%9C%A0%EB%8B%88%EC%BD%94%EB%93%9C-%EC%9E%90%EC%86%8C-%EB%B6%84%EB%A6%AC-%EB%B0%A9%EB%B2%95

초성: 19
["ㄱ", "ㄲ", "ㄴ", "ㄷ", "ㄸ", "ㄹ", "ㅁ", "ㅂ", "ㅃ", "ㅅ", "ㅆ", "ㅇ", "ㅈ", "ㅉ", "ㅊ", "ㅋ", "ㅌ", "ㅍ", "ㅎ"]
중성: 21
["ㅏ", "ㅐ", "ㅑ", "ㅒ", "ㅓ", "ㅔ", "ㅕ", "ㅖ", "ㅗ", "ㅘ", "ㅙ", "ㅚ", "ㅛ", "ㅜ", "ㅝ", "ㅞ", "ㅟ", "ㅠ", "ㅡ", "ㅢ", "ㅣ"] 
종성: 28
["", "ㄱ", "ㄲ", "ㄳ", "ㄴ", "ㄵ", "ㄶ", "ㄷ", "ㄹ", "ㄺ", "ㄻ", "ㄼ", "ㄽ", "ㄾ", "ㄿ", "ㅀ", "ㅁ", "ㅂ", "ㅄ", "ㅅ", "ㅆ", "ㅇ", "ㅈ", "ㅊ", "ㅋ", "ㅌ", "ㅍ", "ㅎ"]

시작은 "가"(0xac00) 이며,
"ㄷ" 는 4번째 초성이며, 조합가능한 총 음절 갯수는 588(1x21x28)개 이다.
"ㅈ" 는 13번째 초성이며, 조합가능한 총 음절 갯수는 588(1x21x28)개 이다.
"""

가 = 0xac00 # 변수 "가" 는 int 임 ;;;
다 = 가 - 1 + (21 * 28 * 3) + 1
딯 = 다 - 1 + (21 * 28)
자 = 가 - 1 + (21 * 28 * 12) + 1
짛 = 자 - 1 + (21 * 28)

ㄷㅈ = [] # 파이썬3 이기에 'ㄷㅈ' 같은 변수명 가능함. 유니코드에 감사드립니다^^^
for i in range(다, 딯 + 1):
    ㄷㅈ.append(chr(i))
for j in range(자, 짛 + 1):
    ㄷㅈ.append(chr(j))
    
"""독불장군은 예외처리하기위하야 '장'을 제외시킴"""
"""이로써 '장'을 제외한 'ㄷ'와 'ㅈ'로 조합가능한 모든 음절을 목록화시켰음: 1175개"""
ㄷㅈ.remove("장") # "장"만 제외

### 지루함 방지용: 커서 순환/반복 ...
"""
Spinner taken from:
https://stackoverflow.com/questions/48854567/python-asynchronous-progress-spinner
"""

import sys, time, threading

class mc:
    읆조림 = [
        "왕의 행차는 서쪽으로 멀어져 가고     ",
        "왕자는 북쪽 땅에서 위태롭다.     ",
        "외로운 신하는 나라를 걱정할 때이고  ",
        "사나이는 공훈을 세워야 할 시기로다. ",
        "바다에 서약하니 물고기와 용이 감동하고",
        "산에 맹세하니 초목이 아는구나         ",
        "원수를 모두 멸할 수 있다면      ",
        "비록 죽음일지라도 사양하지 않으리라.   ",
]

def spin_cursor():
    while True:
        for cursor in mc.읆조림:
            sys.stdout.write(cursor)
            sys.stdout.flush()
            time.sleep(1.6) # 속도 조절
            sys.stdout.write("\b" * 42)
            if done:
                sys.stdout.flush()
                sys.stdout.write(" " * 42)
                sys.stdout.write("\n")
                return

done = False
spin_thread = threading.Thread(target=spin_cursor)
spin_thread.start()

# 여기서 부터 시간 걸리는 작업 시작...

"""이제 지랄같은 'ㄷ','ㅈ' 와 만날때 발음이 변형되는것을 우선 적용함"""
"""아따 코드가 짜파게티/볶음밥/개판소판오분전 햐..."""
for v in range(0, len(data)): 
    vdx = []
    if data[v] == "不" and get_ipa(data[v+1]) in ㄷㅈ:
        vdx.append(v)
        list_data = list(data)
        for z in vdx:
            list_data[z] = "부"
        data = "".join(list_data)

for k in list_hanja: # 나머지 한자들도 한글로 모두 변경함
    if get_ipa(k) == None: # wiktionary에 없는 글자도 존재함
        pass
    elif k not in data and k != "不":
        pass
    else:
        data = data.replace(k, get_ipa(k))

# 시간 걸리는 작업 완료.
done = True
spin_thread.join()

### 다음 작업 계속 진행...

# POSIX 터미널 글자 색깔:
ENDC = "\033[0m"
BOLD = "\033[1m"
UNDERLINE = "\033[4m"
    
for sdx in range(0, len(data)): # 마지막 결과물 화면에 뿌림,,, 끝나따...!!!
    """한자에서 한글로 교체된 부분은 굵게 표시함"""
    if sdx in list_hanja_idx:
        p = subprocess.Popen(['env'], stdout=subprocess.PIPE, text=True)
        r = p.stdout.read()
        if "CHROMEOS_RELEASE_VERSION" in r: # 크롬OS 에선 밑줄 표시
            print(data[sdx].replace(data[sdx], "{0}{1}{2}"\
                                       .format(UNDERLINE,data[sdx],ENDC)), end="")
        else: # 우분투에선 굵게 표시
            print(data[sdx].replace(data[sdx], "{0}{1}{2}"\
                                       .format(BOLD,data[sdx],ENDC)), end="")
    else:
        print(data[sdx], end="")

""" 우리 역사 바로 세우기 """
""" https://www.youtube.com/watch?v=JnAswwKaSDY """
sys.stdout.write("{0}{1}{2}{3}{4}"\
                 .format("\n",UNDERLINE,"경자年 한일전 총선은 4월 15일 입니다.",ENDC,"\n\n"))

# 편집: Emacs 26.2 (Ubuntu 18.04)
# 마지막 갱신: 2019년 9월 30일
